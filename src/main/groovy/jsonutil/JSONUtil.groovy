package jsonutil

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.io.IOException
import java.io.Writer
import java.util.*

// TODO:
// yaml() mode
// json() mode

// permissive deserialization
// pretty serialization
// shorter class name: Parse? From? To? From.json()

@Slf4j
@CompileStatic
final class JSONUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper()

    private static final ObjectMapper prettyMapper = getPrettyMapper()


    static ObjectMapper getPrettyMapper() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        return mapper
    }


    static String serialize(Object o) {
        try {
            return objectMapper.writeValueAsString(o)
        } catch (JsonProcessingException jpe) {
            throw new RuntimeException("JSON serialization mapping error", jpe)
        }
    }


    static void serialize(Object o, Writer writer) {
        try {
            objectMapper.writeValue(writer, o)
        } catch (JsonProcessingException jpe) {
            throw new RuntimeException("JSON serialization mapping error", jpe)
        } catch (IOException ioe) {
            throw new RuntimeException("I/O error", ioe)
        }

    }


    static String logJSON(Object o) {
        try {
            return serialize(o)
        } catch (Exception e) {
            log.trace("log serialization error", e)
            return "JSON ERROR"
        }
    }


    static Object deserialize(String json, Class clazz) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        try {
            return objectMapper.readValue(json, clazz)
        } catch (JsonParseException jpe) {
            throw new RuntimeException("JSON deserialization parse error on (type: " + (clazz == null ? null : clazz.toString()) + "):: " + json, jpe)
        } catch (JsonMappingException jme) {
            throw new RuntimeException("JSON deserialization mapping error on (type: " + (clazz == null ? null : clazz.toString()) + "):: " + json, jme)
        } catch (IOException ioe) {
            throw new RuntimeException("JSON deserialization I/O error on (type: " + (clazz == null ? null : clazz.toString()) + "):: " + json, ioe)
        }
    }


    static Object deserialize(String json, TypeReference<?> typeref) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        try {
            return objectMapper.readValue(json, typeref)
        } catch (JsonParseException jpe) {
            throw new RuntimeException("JSON deserialization parse error on (type: " + (typeref == null ? null : typeref.getType().toString()) + ") :: " + json, jpe)
        } catch (JsonMappingException jme) {
            throw new RuntimeException("JSON deserialization mapping error on (type: " + (typeref == null ? null : typeref.getType().toString()) + "):: " + json, jme)
        } catch (IOException ioe) {
            throw new RuntimeException("JSON deserialization I/O error on deserialize for (type: " + (typeref == null ? null : typeref.getType().toString()) + "):: " + json, ioe)
        }
    }


    static List<String> deserializeList(String json) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        List<String> strs = (List<String>) JSONUtil.deserialize(json, new TypeReference<List<Object>>() {
        })
        return strs
    }


    static List<String> deserializeStringList(String json) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        List<String> strs = (List<String>) JSONUtil.deserialize(json, new TypeReference<List<String>>() {
        })
        return strs
    }


    static Set<String> deserializeStringSet(String json) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        Set<String> strs = (Set<String>) JSONUtil.deserialize(json, new TypeReference<Set<String>>() {
        })
        return strs
    }


    static Map<String, Object> deserializeMap(String json) {
        if (StringUtils.isBlank(json)) {
            return Collections.emptyMap()
        }
        return (Map<String, Object>) deserialize(json, new TypeReference<Map<String, Object>>() {
        })
    }


    static String toJSON(Object o) {
        try {
            return objectMapper.writeValueAsString(o)
        } catch (Exception e) {
            return null
        }

    }


    static String toJSONPretty(Object o) {
        try {
            return prettyMapper.writeValueAsString(o)
        } catch (Exception e) {
            return null
        }
    }

    static String toJSONPrettyList(Object o) {
        StringBuilder sb = new StringBuilder()
        try {
            if (o instanceof Iterable) {
                sb.append("[\n")
                boolean first = true
                for (Object i : (Iterable) o) {
                    if (first) first = false
                    else sb.append(",\n")
                    sb.append(JSONUtil.toJSON(i))
                }
                sb.append("]\n")
            }
            return sb.toString()
        } catch (Exception e) {
            return null
        }
    }


    static Map fromJSON(String json) {
        try {
            if (StringUtils.isBlank(json)) return new HashMap()
            return (Map)objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
            })
        } catch (Exception e) {
            return null
        }
    }

    static Map fromJSON(File jsonFile) {
        try {
            String json = jsonFile?.text
            if (StringUtils.isBlank(json)) return new HashMap()
            return (Map)objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
            })
        } catch (Exception e) {
            return null
        }
    }



    static List fromJSONList(String json) {
        try {
            if (StringUtils.isBlank(json)) return new ArrayList()
            return (List)objectMapper.readValue(json, new TypeReference<List<String>>() {
            })
        } catch (Exception e) {
            return null
        }

    }


    static List fromJSONArrayOfMap(String json) {
        try {
            if (StringUtils.isBlank(json)) return new ArrayList()
            return (List)objectMapper.readValue(json, new TypeReference<List<Map<String, Object>>>() {
            })
        } catch (Exception e) {
            return null
        }

    }

    static String toGroovy(Map<String, Object> map) {
        map?.toMapString()
    }

    static String toGroovy(List<Object> list) {
        list?.toListString()
    }

    static Object fromGroovy(String groovyData) {
        groovyData ? Eval.me(groovyData) : null
    }

    private JSONUtil() {
        // hide constructor for utility class
    }

}
