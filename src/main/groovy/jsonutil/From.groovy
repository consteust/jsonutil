package jsonutil

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils


// TODO: readers/writes/streams
// TODO: properties, xml, csv
// TODO: deserialize to list/map iterators

@CompileStatic
class ToText {
    static String groovy(Object o) {
        if (o instanceof Collection) {
            return ((Collection) o).toListString()
        }
        if (o?.class?.isArray()) {
            return ((Object[]) o).toArrayString()
        }
        if (o instanceof Map) {
            return ((Map) o).toMapString()
        }
        return FromText.json().asMap(json(o)).toMapString()
    }

    static String json(Object o) {
        Mappers.strictJsonMapper.writeValueAsString(o)
    }

    static String jsonPretty(Object o) {
        Mappers.strictJsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o)
    }

    static String yaml(Object o) {
        Mappers.yamlMapper.writeValueAsString(o)
    }
}

@CompileStatic
class FromText {
    static Deserializer json() { JacksonDeserializer.loose() }

    static Deserializer jsonLoose() { JacksonDeserializer.loose() }

    static Deserializer jsonStrict() { JacksonDeserializer.strict() }

    static Deserializer yaml() { JacksonDeserializer.yaml() }

    static Deserializer groovy() { new GroovyDeserializer() }
}

@CompileStatic
class FromJsonLoose {
    static Deserializer deserializer = JacksonDeserializer.loose()

    static Object from(String text, Class toClass) {
        deserializer.from(text, toClass)
    }

    static Object from(String text, TypeReference<?> typeRef) {
        deserializer.from(text, typeRef)
    }

    static Set asSet(String text) {
        deserializer.asSet(text)
    }

    static List asList(String text) {
        deserializer.asList(text)
    }

    static Map asMap(String text) {
        deserializer.asMap(text)
    }
}

@CompileStatic
class FromJsonStrict {
    static Deserializer deserializer = JacksonDeserializer.strict()

    static Object from(String text, Class toClass) {
        deserializer.from(text, toClass)
    }

    static Object from(String text, TypeReference<?> typeRef) {
        deserializer.from(text, typeRef)
    }

    static Set asSet(String text) {
        deserializer.asSet(text)
    }

    static List asList(String text) {
        deserializer.asList(text)
    }

    static Map asMap(String text) {
        deserializer.asMap(text)
    }
}


@CompileStatic
class FromYaml {
    static Deserializer deserializer = JacksonDeserializer.yaml()

    static Object from(String text, Class toClass) {
        deserializer.from(text, toClass)
    }

    static Object from(String text, TypeReference<?> typeRef) {
        deserializer.from(text, typeRef)
    }

    static Set asSet(String text) {
        deserializer.asSet(text)
    }

    static List asList(String text) {
        deserializer.asList(text)
    }

    static Map asMap(String text) {
        deserializer.asMap(text)
    }

}

@CompileStatic
class FromGroovy {
    static Deserializer deserializer = new GroovyDeserializer()

    static Object from(String text, Class toClass) {
        deserializer.from(text, toClass)
    }

    static Object from(String text, TypeReference<?> typeRef) {
        deserializer.from(text, typeRef)
    }

    static Set asSet(String text) {
        deserializer.asSet(text)
    }

    static List asList(String text) {
        deserializer.asList(text)
    }

    static Map asMap(String text) {
        deserializer.asMap(text)
    }
}

@PackageScope
@CompileStatic
interface Deserializer {

    Object from(String text, Class toClass)

    Object from(String text, TypeReference<?> typeRef)

    Set asSet(String text)

    List asList(String text)

    Map asMap(String text)
}

@PackageScope
@CompileStatic
@Slf4j
class JacksonDeserializer implements Deserializer {

    ObjectMapper mapper = Mappers.looseJsonMapper

    void setMode(String mode) {
        switch (mode) {
            case "strict-json":
                mapper = Mappers.strictJsonMapper
                break;
            case "yaml":
                mapper = Mappers.yamlMapper
                break;
            case "loose-json":
            default:
                mapper = Mappers.looseJsonMapper
        }
    }

    Object from(String json, Class toClass) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        try {
            return mapper.readValue(json, toClass)
        } catch (JsonParseException jpe) {
            throw new RuntimeException("JSON deserialization parse error on (type: ${toClass?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), jpe)
        } catch (JsonMappingException jme) {
            throw new RuntimeException("JSON deserialization mapping error on (type: ${toClass?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), jme)
        } catch (IOException ioe) {
            throw new RuntimeException("JSON deserialization I/O error on (type: ${toClass?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), ioe)
        }
    }

    Object from(String json, TypeReference<?> typeRef) {
        if (StringUtils.isBlank(json)) {
            return null
        }
        try {
            return mapper.readValue(json, typeRef)
        } catch (JsonParseException jpe) {
            throw new RuntimeException("JSON deserialization parse error on (type: ${typeRef?.type?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), jpe)
        } catch (JsonMappingException jme) {
            throw new RuntimeException("JSON deserialization mapping error on (type: ${typeRef?.type?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), jme)
        } catch (IOException ioe) {
            throw new RuntimeException("JSON deserialization I/O error on (type: ${typeRef?.type?.toString()}) :: " +
                    (log.isTraceEnabled() ? json : json?.length() > 100 ? json.substring(0, 100) : json), ioe)
        }
    }

    Set asSet(String json) {
        (Set) (StringUtils.isBlank(json) ? null : from(json, Mappers.setTypeReference))
    }

    List asList(String json) {
        (List) (StringUtils.isBlank(json) ? null : from(json, Mappers.listTypeReference))
    }

    Map asMap(String json) {
        (Map) (StringUtils.isBlank(json) ? null : from(json, Mappers.mapTypeReference))
    }

    static Deserializer loose() {
        new JacksonDeserializer(mode: "loose-json")
    }

    static Deserializer strict() {
        new JacksonDeserializer(mode: "strict-json")
    }

    static Deserializer yaml() {
        new JacksonDeserializer(mode: "yaml")
    }

}

@PackageScope
@CompileStatic
class GroovyDeserializer implements Deserializer {

    Object from(String groovyData, Class toClass) {
        Object o = Eval.me(groovyData)
        if (o == null) {
            return null
        }
        if (o.class.isInstance(toClass)) {
            return o
        }
        throw new IllegalArgumentException("Groovy data could not be converted to " + toClass)
    }


    @CompileDynamic
    Object from(String groovyData, TypeReference<?> typeRef) {
        Object o = Eval.me(groovyData)
        if (o == null) {
            return null
        }
        return o
    }


    Set asSet(String groovyData) {
        Object o = Eval.me(groovyData)
        if (o instanceof Collection) {
            return ((Collection) o).toSet()
        }
        throw new IllegalArgumentException("Groovy data could not be converted to a set")
    }


    List asList(String groovyData) {
        Object o = Eval.me(groovyData)
        if (o instanceof Collection) {
            return ((Collection) o).toList()
        }
        throw new IllegalArgumentException("Groovy data could not be converted to a list")
    }


    Map asMap(String groovyData) {
        Object o = Eval.me(groovyData)
        if (o instanceof Map) {
            return (Map) o
        }
        throw new IllegalArgumentException("Groovy data could not be converted to a map")
    }
}