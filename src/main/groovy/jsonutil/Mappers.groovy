package jsonutil

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import groovy.transform.CompileStatic
import groovy.transform.PackageScope

@PackageScope
@CompileStatic
class Mappers {

    static final ObjectMapper strictJsonMapper = new ObjectMapper()
    static final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory())
    static final ObjectMapper looseJsonMapper =  looseMapper()
    static TypeReference<Map<String, Object>> mapTypeReference = new TypeReference<Map<String, Object>>() {}
    static TypeReference<List<Object>> listTypeReference = new TypeReference<List<Object>>() {}
    static TypeReference<Set<Object>> setTypeReference = new TypeReference<Set<Object>>() {}

    static ObjectMapper looseMapper() {
        ObjectMapper om = new ObjectMapper()
        om.configure(JsonParser.Feature.ALLOW_COMMENTS, true)
        om.configure(JsonParser.Feature.ALLOW_TRAILING_COMMA, true)
        om.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true)
        om.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
        return om
    }
}


