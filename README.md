# jsonutil

A wrapper class for convenience methods using jackson

Using jitpack:

    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>

    <dependency>
        <groupId>com.github.cem-util</groupId>
        <artifactId>jsonutil</artifactId>
        <version>1.0</version>
    </dependency>

gradle:

	repositories {
			...
	    maven { url 'https://jitpack.io' }
    }

    compile group: 'com.github.cem-util', name: 'jsonutil', version: 'master-SNAPSHOT'
